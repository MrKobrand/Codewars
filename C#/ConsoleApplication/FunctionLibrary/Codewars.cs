﻿using System;
using System.Collections.Generic;

namespace FunctionLibrary
{
    public class Codewars
    {
        /// <summary>
        /// Converts string to Jaden-Case.
        /// </summary>
        /// <param name="phrase">String which will be converted to Jaden-Case.</param>
        /// <returns>String which is converted to Jaden-Case.</returns>
        public static string ToJadenCase(string phrase)
        {
            char[] chArr = phrase.ToCharArray();
            if (!char.IsUpper(chArr[0]))
                chArr[0] = char.ToUpper(chArr[0]);
            for (int i = phrase.IndexOf(' ', 0); phrase.IndexOf(' ', i) + 1 != 0; i++)
                chArr[phrase.IndexOf(' ', i) + 1] = char.ToUpper(chArr[phrase.IndexOf(' ', i) + 1]);
            return string.Concat(chArr);
        }

        /// <summary>
        /// Searches for a number which appears an odd number of times.
        /// </summary>
        /// <param name="seq">A sequence of integers which is being searching for a number which appears an odd number of times.</param>
        /// <returns>A number which appears an odd number of times.</returns>
        public static int FindIt(int[] seq)
        {
            int[] newArr;
            foreach (int el in seq)
            {
                newArr = Array.FindAll(seq, x => x == el);
                if (newArr.Length % 2 != 0)
                    return newArr[0];
            }

            return -1;
        }

        /// <summary>
        /// Gets sum of all the digits in a number.
        /// </summary>
        /// <param name="n">A number, the sum of the digits of which is being calculated.</param>
        /// <returns>A number, the sum of the digits of which is calculated.</returns>
        public static int SumOfDigits(int n)
        {
            if (n < 10)
                return n;
            if (n % 10 + SumOfDigits(n / 10) >= 10)
                return SumOfDigits(n % 10 + SumOfDigits(n / 10));
            return n % 10 + SumOfDigits(n / 10);
        }

        /// <summary>
        /// Takes an integer (n > 1) and returns an array with all of the integer's divisors.
        /// </summary>
        /// <param name="n">The number which all of it's divisors are looking for.</param>
        /// <returns>An Array of integers containing all divisors of the entered number.</returns>
        public static int[] Divisors(int n)
        {
            var divisors = new List<int>();

            for (var i = 2; i <= n / 2; i++)
                if (n % i == 0)
                    divisors.Add(i);

            return divisors.Count > 0 ? divisors.ToArray() : null;
        }
    }
}