'use strict'

/**
   * Finds the highest and lowest integers in the string.
   * @param strNumbers A string containing integers separated by spaces.
   * @returns A string containing the highest and lowest integers separated by a single space.
   */
const highAndLow = (strNumbers: string): string => {
	const intNumbers: number[] = strNumbers.split(' ').map(strInt => +strInt)
	return `${Math.max(...intNumbers)} ` +
		`${Math.min(...intNumbers)}`
}

/**
   * Finds the strings from firstArr which are substrings of strings of secondArr.
   * @param firstArr An array of substrings in secondArr string array.
   * @param secondArr An array of strings.
   * @returns An array of strings containing in lexicographical order of the strings of firstArr which are substrings of 			strings of secondArr.
   */
function inArray(firstArr: string[], secondArr: string[]): string[] {
	let resultArr: string[] = []

	firstArr.forEach((substr) => {
		secondArr.forEach((str) => {
			if (str.includes(substr))
				if (resultArr.indexOf(substr) === -1)
					resultArr.push(substr)
		})
	})

	return resultArr.sort()
}

// console.log(highAndLow('4 5 29 54 4 0 -214 542 -64 1 -3 6 -6'))
console.log(inArray(["live", "strong", "arp"], ["lively", "alive", "harp", "sharp", "armstrong"]))