from typing import List


# An area of a right triangle
def t_area(t_str: str) -> float:
    strArr = t_str.split('\n')

    for el in strArr:
        if el == '':
            strArr.remove(el)

    height: int = len(strArr) - 1
    width: int = strArr[-1].count('.') - 1

    return height * width / 2


# Sort the odd: [5, 3, 2, 8, 1, 4] -> [1, 3, 2, 8, 5, 4]
def sort_array(source_array: List[int]) -> List[int]:
    dictOddInt = dict()

    for i in range(len(source_array)):
        if source_array[i] % 2 == 1:
            dictOddInt[i] = source_array[i]

    listOddInt = list(dictOddInt.values())
    listOddInt.sort()

    for i in range(len(source_array)):
        if i in dictOddInt.keys():
            source_array[i] = listOddInt.pop(0)

    return source_array


