import Codewars


def main() -> None:
    # Calling functions
    # print(Codewars.t_area('\n.\n. .\n. . .\n'))

    print(Codewars.sort_array([5, 3, 2, 8, 1, 4]))


if __name__ == '__main__':
    main()

